package com.jeramtough.im.action.controller;


import com.jeramtough.im.model.dto.ImParentDto;
import com.jeramtough.im.model.dto.ImParentWithStudentDto;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.params.AddOrUpdateParentParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.model.params.DeleteByIdsParams;
import com.jeramtough.im.service.ImParentService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-17
 */
@Api(tags = {"Im家长API"})
@RestController
@RequestMapping("/imParent")
public class ImParentController extends MyBaseController {

    private final ImParentService thisService;

    @Autowired
    public ImParentController(ImParentService imStudentService) {
        this.thisService = imStudentService;
    }

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public CommonApiResponse<String> add(
            @RequestBody AddOrUpdateParentParams params) {
        return getSuccessfulApiResponse(thisService.addParent(params));
    }

    @ApiOperation(value = "更新", notes = "更新")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public CommonApiResponse<String> update(
            @RequestBody AddOrUpdateParentParams params) {
        return getSuccessfulApiResponse(thisService.updateParent(params));
    }

    @ApiOperation(value = "删除", notes = "删除信息")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public CommonApiResponse<Boolean> delete(@RequestBody DeleteByIdsParams params) {
//        return getSuccessfulApiResponse(true);
        return getSuccessfulApiResponse(thisService.removeByIds(params.getIds()));
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/page", method = {RequestMethod.GET})
    public CommonApiResponse<PageDto<ImParentDto>> page(
            QueryByPageParams queryByPageParams, BaseConditionParams conditionParams) {
        return getSuccessfulApiResponse(
                thisService.pageByCondition(queryByPageParams, conditionParams));
    }

    @ApiOperation(value = "全部", notes = "查询全部,学生与父母关联关系")
    @RequestMapping(value = "/all", method = {RequestMethod.GET})
    public CommonApiResponse<List<ImParentWithStudentDto>> all() {
        return getSuccessfulApiResponse(
                thisService.getAll());
    }

}

