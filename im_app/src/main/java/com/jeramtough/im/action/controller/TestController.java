package com.jeramtough.im.action.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * Created on 2020/11/25 0:44
 * by @author WeiBoWen
 * </pre>
 */
@Api(tags = {"Test"})
@RestController
@RequestMapping("/test")
public class TestController {


    @RequestMapping(value = "/test1")
    @ResponseBody
    public String test() {
        return "test";
    }
}


