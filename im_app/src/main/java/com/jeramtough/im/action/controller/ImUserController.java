package com.jeramtough.im.action.controller;

import com.jeramtough.im.service.UserService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <pre>
 * Created on 2021/8/17 下午9:58
 * by @author WeiBoWen
 * </pre>
 */
@RestController
@Api(tags = {"用户接口"})
@RequestMapping("/user")
public class ImUserController extends MyBaseController {

    private final UserService userService;

    @Autowired
    public ImUserController(UserService userService) {
        this.userService = userService;
    }


    @ApiOperation(value = "查询登录用户信息", notes = "根据令牌查询登录用户信息")
    @RequestMapping(value = "/info", method = {RequestMethod.GET})
    @ApiResponses(value = {
    })
    public CommonApiResponse<Map<String, Object>> info() {
        return getSuccessfulApiResponse(userService.getInfo());
    }

}
