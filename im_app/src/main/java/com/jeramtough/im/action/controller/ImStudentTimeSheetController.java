package com.jeramtough.im.action.controller;


import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.dto.ImStudentTimeSheetDto;
import com.jeramtough.im.model.params.AddOrUpdateImStudentTimeSheetParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentScoreParams;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.im.model.params.StudentTimeSheetConditionParams;
import com.jeramtough.im.service.ImStudentScoreService;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.ImStudentTimeSheetService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@Api(tags = {"Im学生考勤API"})
@RestController
@RequestMapping("/imStudentTimeSheet")
public class ImStudentTimeSheetController extends MyBaseController {


    private final ImStudentTimeSheetService thisService;

    @Autowired
    public ImStudentTimeSheetController(
            ImStudentTimeSheetService baseService) {
        this.thisService = baseService;
    }


    @ApiOperation(value = "更新", notes = "更新")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public CommonApiResponse<String> update(
            @RequestBody List<AddOrUpdateImStudentTimeSheetParams> params) {
        return getSuccessfulApiResponse(thisService.updateStudentTimeSheet(params));
    }

    @ApiOperation(value = "获取当月所有日期key", notes = "分页查询")
    @RequestMapping(value = "/dayKeys", method = {RequestMethod.GET})
    public CommonApiResponse<List<String>> dayKeys(Date date) {
        return getSuccessfulApiResponse(
                thisService.getDayKeys(date));
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/page", method = {RequestMethod.GET})
    public CommonApiResponse<PageDto<Map<String, Object>>> page(
            QueryByPageParams queryByPageParams,
            StudentTimeSheetConditionParams conditionParams) {
        return getSuccessfulApiResponse(
                thisService.pageByStudentTimeSheetCondition(queryByPageParams,
                        conditionParams));
    }

}

