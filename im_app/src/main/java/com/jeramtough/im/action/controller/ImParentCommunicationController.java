package com.jeramtough.im.action.controller;


import com.jeramtough.im.model.dto.ImParentCommunicationDto;
import com.jeramtough.im.model.dto.ImParentDto;
import com.jeramtough.im.model.params.AddOrUpdateParentCommunicationParams;
import com.jeramtough.im.model.params.AddOrUpdateParentParams;
import com.jeramtough.im.model.params.DeleteByIdsParams;
import com.jeramtough.im.model.params.ParentCommunicationConditionParams;
import com.jeramtough.im.service.ImParentCommunicationService;
import com.jeramtough.im.service.ImParentService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@Api(tags = {"Im家长沟通API"})
@RestController
@RequestMapping("/imParentCommunication")
public class ImParentCommunicationController extends MyBaseController {

    private final ImParentCommunicationService thisService;

    @Autowired
    public ImParentCommunicationController(
            ImParentCommunicationService imParentCommunicationService) {
        this.thisService = imParentCommunicationService;
    }

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public CommonApiResponse<String> add(
            @RequestBody AddOrUpdateParentCommunicationParams params) {
        return getSuccessfulApiResponse(thisService.addImParentCommunication(params));
    }

    @ApiOperation(value = "更新", notes = "更新")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public CommonApiResponse<String> update(
            @RequestBody AddOrUpdateParentCommunicationParams params) {
        return getSuccessfulApiResponse(thisService.updateByParams(params));
    }

    @ApiOperation(value = "删除", notes = "删除信息")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public CommonApiResponse<Boolean> delete(@RequestBody DeleteByIdsParams params) {
//        return getSuccessfulApiResponse(true);
        return getSuccessfulApiResponse(thisService.removeByIds(params.getIds()));
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/page", method = {RequestMethod.GET})
    public CommonApiResponse<PageDto<ImParentCommunicationDto>> page(
            QueryByPageParams queryByPageParams,
            ParentCommunicationConditionParams conditionParams) {
        return getSuccessfulApiResponse(
                thisService.pageByParentCommunicationCondition(queryByPageParams,
                        conditionParams));
    }
}

