package com.jeramtough.im.action.filter;

import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.component.userdetail.login.UserTokenCacheLoginer;
import com.jeramtough.im.component.userdetail.login.UserTokenLoginer;
import com.jeramtough.jtweb.action.filter.AbastractSwaggerFilter;
import com.jeramtough.jtweb.action.filter.BaseSwaggerFilter;
import com.jeramtough.jtweb.component.apiresponse.error.ErrorCode;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiException;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseException;
import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;
import com.jeramtough.randl2.sdk.token.JwtTokenRequestParser;
import com.jeramtough.randl2.sdk.token.TokenRequestParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * <pre>
 * Created on 2021/8/17 下午11:06
 * by @author WeiBoWen
 * </pre>
 */
@WebFilter(urlPatterns = {"/*", ""}, filterName = "uerTokenFilter")
public class UserTokenFilter extends AbastractSwaggerFilter {

    private final UserTokenCacheLoginer userTokenLoginer;


    private static final String[] OPENED_ADI_URLS = {
            "/access/**",
    };

    @Autowired
    public UserTokenFilter(
            UserTokenCacheLoginer userTokenLoginer) {
        this.userTokenLoginer = userTokenLoginer;
    }

    @Override
    public boolean isOpenStaticResource() {
        return false;
    }

    @Override
    public String[] getOponUrls() {
        return OPENED_ADI_URLS;
    }


    @Override
    public void doFilter2(ServletRequest servletRequest, ServletResponse servletResponse,
                          FilterChain filterChain) {

        TokenRequestParser tokenRequestParser = new JwtTokenRequestParser();
        String authorizationHeader =
                tokenRequestParser.getAuthorizationHeader((HttpServletRequest) servletRequest);

        if (authorizationHeader == null) {
            returnCommonApiResponse(
                    getFailedApiResponse(new ApiException(ErrorCode.U, "token请求头" +
                            "未提供！")),
                    (HttpServletResponse) servletResponse);
        }

        try {
            SystemUser systemUser = userTokenLoginer.login(authorizationHeader);
            UserHolder.afterLogin(systemUser);
            filterChain.doFilter(servletRequest, servletResponse);
        }
        catch (Exception e) {
            if (!(e instanceof ApiResponseException)) {
                e.printStackTrace();
            }
            returnCommonApiResponse(getFailedApiResponse(e),
                    (HttpServletResponse) servletResponse);
        }
    }
}
