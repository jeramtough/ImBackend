package com.jeramtough.im.action.controller;


import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.params.AddOrUpdateStudentScoreParams;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.im.service.ImStudentScoreService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
@Api(tags = {"Im学生成绩API"})
@RestController
@RequestMapping("/imStudentScore")
public class ImStudentScoreController extends MyBaseController{

    private final ImStudentScoreService thisService;

    @Autowired
    public ImStudentScoreController(
            ImStudentScoreService baseService) {
        this.thisService = baseService;
    }


    @ApiOperation(value = "更新", notes = "更新")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public CommonApiResponse<String> update(
            @RequestBody List<AddOrUpdateStudentScoreParams> params) {
        return getSuccessfulApiResponse(thisService.updateStudentScore(params));
    }


    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/page", method = {RequestMethod.GET})
    public CommonApiResponse<PageDto<ImStudentScoreDto>> page(
            QueryByPageParams queryByPageParams, StudentScoreConditionParams conditionParams) {
        return getSuccessfulApiResponse(
                thisService.pageByStudentScoreCondition(queryByPageParams, conditionParams));
    }



}

