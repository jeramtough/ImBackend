package com.jeramtough.im.action.controller;


import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.params.DeleteByIdsParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.model.params.StudentConditionParams;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author WeiJieHui
 * @since 2021-08-24
 */
@Api(tags = {"Im学生API"})
@RestController
@RequestMapping("/imStudent")
public class ImStudentController extends MyBaseController {

    private final ImStudentService thisService;

    @Autowired
    public ImStudentController(ImStudentService imStudentService) {
        this.thisService = imStudentService;
    }

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public CommonApiResponse<String> add(
            @RequestBody AddOrUpdateStudentParams params) {
        return getSuccessfulApiResponse(thisService.addStudent(params));
    }

    @ApiOperation(value = "更新", notes = "更新")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public CommonApiResponse<String> update(
            @RequestBody AddOrUpdateStudentParams params) {
        return getSuccessfulApiResponse(thisService.updateStudent(params));
    }

    @ApiOperation(value = "删除", notes = "删除学生信息")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public CommonApiResponse<Boolean> delete(@RequestBody DeleteByIdsParams params) {
//        return getSuccessfulApiResponse(true);
        return getSuccessfulApiResponse(thisService.removeByIds(params.getIds()));
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/page", method = {RequestMethod.GET})
    public CommonApiResponse<PageDto<ImStudentDto>> page(
            QueryByPageParams queryByPageParams, StudentConditionParams conditionParams) {
        return getSuccessfulApiResponse(
                thisService.pageByCondition(queryByPageParams, conditionParams));
    }

    @ApiOperation(value = "全部", notes = "查询全部")
    @RequestMapping(value = "/all", method = {RequestMethod.GET})
    public CommonApiResponse<List<ImStudentDto>> all() {
        return getSuccessfulApiResponse(
                thisService.getAll());
    }

    @ApiOperation(value = "全部班级", notes = "查询全部班级")
    @RequestMapping(value = "/classNumbers", method = {RequestMethod.GET})
    public CommonApiResponse<List<String>> getStudentClassNumbers() {
        return getSuccessfulApiResponse(
                thisService.getStudentClassNumbers());
    }


}

