package com.jeramtough.im.action.controller;

import com.jeramtough.im.model.MyErrorU;
import com.jeramtough.jtweb.action.controller.BaseSwaggerController;
import com.jeramtough.jtweb.model.error.ErrorS;
import com.jeramtough.jtweb.model.error.ErrorU;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * <pre>
 * Created on 2020/2/6 14:56
 * by @author JeramTough
 * </pre>
 */
@ApiResponses({
        @ApiResponse(code = MyErrorU.CODE_601.C, message = MyErrorU.CODE_601.M),
        @ApiResponse(code = MyErrorU.CODE_602.C, message = MyErrorU.CODE_602.M),
        @ApiResponse(code = MyErrorU.CODE_603.C, message = MyErrorU.CODE_603.M),
        @ApiResponse(code = MyErrorU.CODE_604.C, message = MyErrorU.CODE_604.M),
})
public abstract class MyBaseController extends BaseSwaggerController {


}
