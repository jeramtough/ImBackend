package com.jeramtough.im.action.controller;

import com.jeramtough.im.model.MyErrorU;
import com.jeramtough.im.service.ImResourceService;
import com.jeramtough.jtweb.component.apiresponse.bean.CommonApiResponse;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Map;

/**
 * <pre>
 * Created on 2021/9/15 上午10:33
 * by @author WeiBoWen
 * </pre>
 */
@Api(tags = {"Sm资源API"})
@RestController
@RequestMapping("/imResource")
public class ImResourceController extends MyBaseController {

    private final ImResourceService imResourceService;

    @Autowired
    public ImResourceController(ImResourceService imResourceService) {
        this.imResourceService = imResourceService;
    }

    @ApiOperation(value = "上传头像", notes = "上传头像")
    @RequestMapping(value = "/headimage/upload", method = RequestMethod.POST,
            headers = {"content-type=multipart/form-data"}, consumes = {"multipart/*"})
    @ApiResponses(value = {
            @ApiResponse(code = MyErrorU.CODE_601.C, message = MyErrorU.CODE_601.M),
            @ApiResponse(code = MyErrorU.CODE_602.C, message = MyErrorU.CODE_602.M),
            @ApiResponse(code = MyErrorU.CODE_603.C, message = MyErrorU.CODE_603.M),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "头像文件", dataType = "MultipartFile",
                    allowMultiple = true,
                    required = true, paramType = "form")})
    public CommonApiResponse<Map<String, Object>> uploadSurfaceImage(
            @RequestParam("file") MultipartFile file) {
        return getSuccessfulApiResponse(imResourceService.uploadSurfaceImage(file));
    }

    @RequestMapping(value = "/headimage/read/{year}/{month}/{day}/{name}", method =
            RequestMethod.GET)
    public void readSurfaceImage(HttpServletResponse response,
                                 @PathVariable("year") String year,
                                 @PathVariable("month") String month,
                                 @PathVariable("day") String day,
                                 @PathVariable("name") String name) {
        String imagePath =
                File.separator + year + File.separator + month + File.separator
                        + day + File.separator + name;
        imResourceService.readSurfaceImage(response, imagePath);
    }

    @RequestMapping(value = "/headimage/base64/{year}/{month}/{day}/{name}", method =
            RequestMethod.GET)
    public CommonApiResponse<String> readSurfaceImageForBase64(@PathVariable("year")
                                                                       String year,
                                                               @PathVariable("month")
                                                                       String month,
                                                               @PathVariable("day") String day,
                                                               @PathVariable("name")
                                                                       String name) {
        String imagePath =
                File.separator + year + File.separator + month + File.separator
                        + day + File.separator + name;
        return getSuccessfulApiResponse(
                imResourceService.readSurfaceImageForBase64(imagePath));
    }

}
