package com.jeramtough.im.util;

import com.jeramtough.jtcomponent.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * Created on 2020/12/17 14:16
 * by @author WeiBoWen
 * </pre>
 */
public class MapTranslateUtil {

    /**
     * 下划线转驼峰命名结果结合
     */
    public static List<Map<String, Object>> transitionName(List<Map<String, Object>> list) {
        List<Map<String, Object>> newList = new ArrayList<>();
        for (Object o : list) {
            Map<String, Object> map = (Map<String, Object>) o;
            Map<String, Object> newMap = new HashMap<>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    String newKey = StringUtil.lineToHump(key);
                    newMap.put(newKey, value);
                }
            }
            newList.add(newMap);
        }

        return newList;
    }

    public static Map<String, Object> transitionName(Map<String, Object> map) {
        Map<String, Object> newMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value != null) {
                String newKey = StringUtil.lineToHump(key);
                newMap.put(newKey, value);
            }
        }
        return newMap;
    }
}
