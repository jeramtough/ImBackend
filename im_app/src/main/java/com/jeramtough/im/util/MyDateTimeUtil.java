package com.jeramtough.im.util;

import cn.hutool.core.date.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <pre>
 * Created on 2021/9/25 下午7:44
 * by @author WeiBoWen
 * </pre>
 */
public class MyDateTimeUtil {

    public static List<Date> getDaysOfMonth(Date date) {
        Date endDate = DateUtil.endOfMonth(date);
        int endDayCount = DateUtil.dayOfMonth(endDate);
        String dayPair = DateUtil.format(date, "yyyy-MM");

        List<Date> dateList = new ArrayList<>();
        for (int i = 1; i <= endDayCount; i++) {
            String dayStr = i + "";
            if (dayStr.length() == 1) {
                dayStr = "0" + dayStr;
            }
            dayStr = dayPair + "-" + dayStr;
            Date dateOfMonth = DateUtil.parse(dayStr, "yyyy-MM-dd");
            dateList.add(dateOfMonth);
        }
        return dateList;
    }


}
