package com.jeramtough.im.config.setting;

import com.jeramtough.randl2.sdk.model.oauth.Oauth2ClientConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * <pre>
 * Created on 2020/9/28 14:38
 * by @author WeiBoWen
 * </pre>
 */
@Configuration
@ConfigurationProperties(prefix = "app.setting")
public class AppSetting {

    private Oauth2ClientConfig oauth2ClientConfig;
    private Map<String, String> randlResourceApi;
    private UploadFileSetting uploadFileSetting;
    private AdministratorSetting administratorSetting;

    public AppSetting() {
    }

    public Oauth2ClientConfig getOauth2ClientConfig() {
        return oauth2ClientConfig;
    }

    public void setOauth2ClientConfig(Oauth2ClientConfig oauth2ClientConfig) {
        this.oauth2ClientConfig = oauth2ClientConfig;
    }


    public Map<String, String> getRandlResourceApi() {
        return randlResourceApi;
    }

    public void setRandlResourceApi(Map<String, String> randlResourceApi) {
        this.randlResourceApi = randlResourceApi;
    }

    public String getRandlResourceApiContextUrl() {
        return getRandlResourceApi().get("context-url");
    }

    public UploadFileSetting getUploadFileSetting() {
        return uploadFileSetting;
    }

    public void setUploadFileSetting(
            UploadFileSetting uploadFileSetting) {
        this.uploadFileSetting = uploadFileSetting;
    }

    public AdministratorSetting getAdministratorSetting() {
        return administratorSetting;
    }

    public void setAdministratorSetting(
            AdministratorSetting administratorSetting) {
        this.administratorSetting = administratorSetting;
    }
}
