package com.jeramtough.im.config.setting;

import com.jeramtough.jtweb.component.filesaver.config.ImageFileSaveConfigAdapter;

/**
 * <pre>
 * Created on 2021/9/15 上午9:22
 * by @author WeiBoWen
 * </pre>
 */
public class Headimage implements ImageFileSaveConfigAdapter {

    private String path;
    private int maxSize;
    private int resetWidth;
    private int resetHeight;
    private String type;
    private String saveExtname;

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public int getResetWidth() {
        return resetWidth;
    }

    @Override
    public int getResetHeight() {
        return resetHeight;
    }

    public void setResetWidth(int resetWidth) {
        this.resetWidth = resetWidth;
    }


    public void setResetHeight(int resetHeight) {
        this.resetHeight = resetHeight;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getSaveExtname() {
        return saveExtname;
    }

    public void setSaveExtname(String saveExtname) {
        this.saveExtname = saveExtname;
    }
}
