package com.jeramtough.im.config.setting;

/**
 * <pre>
 * Created on 2021/9/15 上午9:21
 * by @author WeiBoWen
 * </pre>
 */
public class UploadFileSetting {

    private Headimage headimage;

    public Headimage getHeadimage() {
        return headimage;
    }

    public void setHeadimage(Headimage headimage) {
        this.headimage = headimage;
    }
}
