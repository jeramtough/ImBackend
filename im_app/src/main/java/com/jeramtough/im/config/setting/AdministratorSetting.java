package com.jeramtough.im.config.setting;

/**
 * <pre>
 * Created on 2021/9/18 上午9:31
 * by @author WeiBoWen
 * </pre>
 */
public class AdministratorSetting {

    private Long roldId;

    public Long getRoldId() {
        return roldId;
    }

    public void setRoldId(Long roldId) {
        this.roldId = roldId;
    }
}
