package com.jeramtough.im.config;

import com.jeramtough.jtweb.component.cache.template.CacheTemplate;
import com.jeramtough.jtweb.component.cache.template.InMemoryCacheTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * Created on 2021/9/16 上午9:08
 * by @author WeiBoWen
 * </pre>
 */
@Configuration
public class JtWebConfig {

    @Bean
    public CacheTemplate getCacheTemplate() {
        return new InMemoryCacheTemplate();
    }

}
