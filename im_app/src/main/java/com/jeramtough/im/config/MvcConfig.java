package com.jeramtough.im.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * Created on 2021/8/18 上午10:27
 * by @author WeiBoWen
 * </pre>
 */
@Configuration
@ServletComponentScan(basePackages = "com.jeramtough.im.action.filter")
public class MvcConfig {

    /*@Bean
    public FilterRegistrationBean timeFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        MyFilter myFilter = new MyFilter();
        registrationBean.setFilter(myFilter);
        ArrayList<String> urls = new ArrayList<>();
        urls.add("/*");//配置过滤规则
        registrationBean.setUrlPatterns(urls);
        return registrationBean;
    }*/

}
