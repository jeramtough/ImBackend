package com.jeramtough.im.model.params;

import java.util.List;

/**
 * <pre>
 * Created on 2021/8/25 上午1:32
 * by @author WeiBoWen
 * </pre>
 */
public class DeleteByIdsParams {

    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
}
