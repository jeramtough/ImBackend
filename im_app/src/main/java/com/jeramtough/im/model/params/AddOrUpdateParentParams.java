package com.jeramtough.im.model.params;

import com.jeramtough.jtweb.model.error.ErrorU;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-17
 */
@ApiModel(value = "ImParent对象", description = "")
public class AddOrUpdateParentParams implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long fid;

    @ApiModelProperty(value = "家长名字")
    @NotNull(payload = ErrorU.CODE_1.class)
    private String name;

    @ApiModelProperty(value = "关系")
    private String relation;

    @ApiModelProperty(value = "工作单位")
    private String organization;

    @ApiModelProperty(value = "联系电话")
    private String phoneNumber;

    @ApiModelProperty(value = "学生学号")
    private String studentNumberOrName;


    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStudentNumberOrName() {
        return studentNumberOrName;
    }

    public void setStudentNumberOrName(String studentNumberOrName) {
        this.studentNumberOrName = studentNumberOrName;
    }

    @Override
    public String toString() {
        return "ImParent{" +
                "fid=" + fid +
                ", name=" + name +
                ", relation=" + relation +
                ", organization=" + organization +
                ", phoneNumber=" + phoneNumber +
                "}";
    }
}
