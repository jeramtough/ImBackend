package com.jeramtough.im.model.params;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeramtough.jtweb.model.error.ErrorU;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <pre>
 * Created on 2021/8/24 上午12:45
 * by @author WeiBoWen
 * </pre>
 */
public class AddOrUpdateStudentScoreParams implements Serializable {

    private static final long serialVersionUID = -8670639093212834949L;

    @ApiModelProperty(value = "主键")
    private Long fid;

    @ApiModelProperty(value = "英语成绩")
    private BigDecimal scoresEnglish;

    @ApiModelProperty(value = "艺术成绩")
    private BigDecimal scoresArt;

    @ApiModelProperty(value = "综合成绩")
    private BigDecimal scoresComprehensive;

    @ApiModelProperty(value = "语文成绩")
    private BigDecimal scoresChinese;

    @ApiModelProperty(value = "数学成绩")
    private BigDecimal scoresMath;

    @ApiModelProperty(value = "学期")
    private String semester;

    @NotNull(payload = ErrorU.CODE_1.class)
    @ApiModelProperty(value = "学生id")
    private Long studentId;

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public BigDecimal getScoresEnglish() {
        return scoresEnglish;
    }

    public void setScoresEnglish(BigDecimal scoresEnglish) {
        this.scoresEnglish = scoresEnglish;
    }

    public BigDecimal getScoresArt() {
        return scoresArt;
    }

    public void setScoresArt(BigDecimal scoresArt) {
        this.scoresArt = scoresArt;
    }

    public BigDecimal getScoresComprehensive() {
        return scoresComprehensive;
    }

    public void setScoresComprehensive(BigDecimal scoresComprehensive) {
        this.scoresComprehensive = scoresComprehensive;
    }

    public BigDecimal getScoresChinese() {
        return scoresChinese;
    }

    public void setScoresChinese(BigDecimal scoresChinese) {
        this.scoresChinese = scoresChinese;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public BigDecimal getScoresMath() {
        return scoresMath;
    }

    public void setScoresMath(BigDecimal scoresMath) {
        this.scoresMath = scoresMath;
    }
}
