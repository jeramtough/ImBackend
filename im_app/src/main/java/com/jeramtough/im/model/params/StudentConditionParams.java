package com.jeramtough.im.model.params;

import com.jeramtough.jtweb.model.params.BaseConditionParams;

import java.util.Date;

/**
 * <pre>
 * Created on 2021/9/20 下午10:30
 * by @author WeiBoWen
 * </pre>
 */
public class StudentConditionParams extends BaseConditionParams {

    private String classNumber;

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

}
