package com.jeramtough.im.model.params;

import com.jeramtough.jtweb.model.params.BaseConditionParams;

/**
 * <pre>
 * Created on 2021/9/20 下午10:30
 * by @author WeiBoWen
 * </pre>
 */
public class ParentCommunicationConditionParams extends BaseConditionParams {

    private String classNumber;
    private String semester;

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
