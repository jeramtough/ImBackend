package com.jeramtough.im.model.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * VIEW
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
public class ImStudentScoreDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "成绩表主键")
    private Long fid;

    @ApiModelProperty(value = "学期名")
    private String semester;

    @ApiModelProperty(value = "主键")
    private Long studentId;

    @ApiModelProperty(value = "操作者用户id")
    private Long operatorUid;

    @ApiModelProperty(value = "真实名字")
    private String realname;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "班级编号")
    private String classNumber;

    @ApiModelProperty(value = "就读学校")
    private String school;

    @ApiModelProperty(value = "入学日期")
    private LocalDate startDate;

    @ApiModelProperty(value = "学号")
    private String number;

    @ApiModelProperty(value = "宿舍号码")
    private String dormitoryNumber;

    @ApiModelProperty(value = "报读专业")
    private String thinkMajor;

    @ApiModelProperty(value = "报读班型")
    private String classType;

    @ApiModelProperty(value = "文理标志")
    private String studyTag;

    @ApiModelProperty(value = "英语成绩")
    private BigDecimal scoresEnglish;

    @ApiModelProperty(value = "艺术成绩")
    private BigDecimal scoresArt;

    @ApiModelProperty(value = "综合成绩")
    private BigDecimal scoresComprehensive;

    @ApiModelProperty(value = "语文成绩")
    private BigDecimal scoresChinese;

    @ApiModelProperty(value = "数学成绩")
    private BigDecimal scoresMath;


    @JSONField(format = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "数据创建时间")
    private LocalDateTime createTime;

    @JSONField(format = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "数据更新时间")
    private LocalDateTime updateTime;

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getOperatorUid() {
        return operatorUid;
    }

    public void setOperatorUid(Long operatorUid) {
        this.operatorUid = operatorUid;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDormitoryNumber() {
        return dormitoryNumber;
    }

    public void setDormitoryNumber(String dormitoryNumber) {
        this.dormitoryNumber = dormitoryNumber;
    }

    public String getThinkMajor() {
        return thinkMajor;
    }

    public void setThinkMajor(String thinkMajor) {
        this.thinkMajor = thinkMajor;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getStudyTag() {
        return studyTag;
    }

    public void setStudyTag(String studyTag) {
        this.studyTag = studyTag;
    }

    public BigDecimal getScoresEnglish() {
        return scoresEnglish;
    }

    public void setScoresEnglish(BigDecimal scoresEnglish) {
        this.scoresEnglish = scoresEnglish;
    }

    public BigDecimal getScoresArt() {
        return scoresArt;
    }

    public void setScoresArt(BigDecimal scoresArt) {
        this.scoresArt = scoresArt;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public BigDecimal getScoresComprehensive() {
        return scoresComprehensive;
    }

    public void setScoresComprehensive(BigDecimal scoresComprehensive) {
        this.scoresComprehensive = scoresComprehensive;
    }

    public BigDecimal getScoresChinese() {
        return scoresChinese;
    }

    public void setScoresChinese(BigDecimal scoresChinese) {
        this.scoresChinese = scoresChinese;
    }

    @Override
    public String toString() {
        return "TempView{" +
                "semester=" + semester +
                ", studentId=" + studentId +
                ", operatorUid=" + operatorUid +
                ", realname=" + realname +
                ", gender=" + gender +
                ", classNumber=" + classNumber +
                ", school=" + school +
                ", startDate=" + startDate +
                ", number=" + number +
                ", dormitoryNumber=" + dormitoryNumber +
                ", thinkMajor=" + thinkMajor +
                ", classType=" + classType +
                ", studyTag=" + studyTag +
                ", scoresEnglish=" + scoresEnglish +
                ", scoresArt=" + scoresArt +
                ", fid=" + fid +
                ", scoresComprehensive=" + scoresComprehensive +
                ", scoresChinese=" + scoresChinese +
                "}";
    }

    public BigDecimal getScoresMath() {
        return scoresMath;
    }

    public void setScoresMath(BigDecimal scoresMath) {
        this.scoresMath = scoresMath;
    }
}
