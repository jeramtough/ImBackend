package com.jeramtough.im.model.params;

import com.jeramtough.jtweb.model.error.ErrorU;
import com.jeramtough.jtweb.model.params.BaseConditionParams;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

/**
 * <pre>
 * Created on 2021/9/20 下午10:30
 * by @author WeiBoWen
 * </pre>
 */
public class StudentTimeSheetConditionParams extends StudentConditionParams {

    /**
     * 考勤日期，年-月
     */
    @NotNull(payload = ErrorU.CODE_1.class)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
