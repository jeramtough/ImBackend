package com.jeramtough.im.model.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 *
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
@ApiModel(value = "ImStudentScore对象", description = "")
public class ImStudentScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "fid", type = IdType.AUTO)
    private Long fid;

    /**
     * 英语成绩
     */
    private BigDecimal scoresEnglish;

    /**
     * 艺术成绩
     */
    private BigDecimal scoresArt;

    /**
     * 综合成绩
     */
    private BigDecimal scoresComprehensive;

    /**
     * 数学成绩
     */
    private BigDecimal scoresMath;

    /**
     * 语文成绩
     */
    private BigDecimal scoresChinese;

    /**
     * 学期
     */
    private String semester;

    /**
     * 学生id
     */
    private Long studentId;


    /**
     * 数据创建时间
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public BigDecimal getScoresEnglish() {
        return scoresEnglish;
    }

    public void setScoresEnglish(BigDecimal scoresEnglish) {
        this.scoresEnglish = scoresEnglish;
    }

    public BigDecimal getScoresArt() {
        return scoresArt;
    }

    public void setScoresArt(BigDecimal scoresArt) {
        this.scoresArt = scoresArt;
    }

    public BigDecimal getScoresComprehensive() {
        return scoresComprehensive;
    }

    public void setScoresComprehensive(BigDecimal scoresComprehensive) {
        this.scoresComprehensive = scoresComprehensive;
    }

    public BigDecimal getScoresChinese() {
        return scoresChinese;
    }

    public void setScoresChinese(BigDecimal scoresChinese) {
        this.scoresChinese = scoresChinese;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ImStudentScore{" +
                "fid=" + fid +
                ", scoresEnglish=" + scoresEnglish +
                ", scoresArt=" + scoresArt +
                ", scoresComprehensive=" + scoresComprehensive +
                ", scoresChinese=" + scoresChinese +
                ", semester=" + semester +
                ", studentId=" + studentId +
                "}";
    }

    public BigDecimal getScoresMath() {
        return scoresMath;
    }

    public void setScoresMath(BigDecimal scoresMath) {
        this.scoresMath = scoresMath;
    }
}
