package com.jeramtough.im.model.params;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeramtough.jtweb.model.error.ErrorU;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <pre>
 * Created on 2021/8/24 上午12:45
 * by @author WeiBoWen
 * </pre>
 */
public class AddOrUpdateStudentParams implements Serializable {

    private static final long serialVersionUID = -8670639093212834949L;

    @ApiModelProperty(value = "主键")
    private Long fid;

    @ApiModelProperty(value = "真实名字")
    @NotNull(payload = ErrorU.CODE_1.class)
    private String realname;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "班级编号")
    private String classNumber;

    @ApiModelProperty(value = "就读学校")
    private String school;

    @ApiModelProperty(value = "入学日期")
    private LocalDate startDate;

    @ApiModelProperty(value = "学号")
    private String number;

    @ApiModelProperty(value = "身份证号")
    private String identityNumber;

    @ApiModelProperty(value = "家庭地址")
    private String homeAddress;

    @ApiModelProperty(value = "宿舍号码")
    private String dormitoryNumber;

    @ApiModelProperty(value = "校服尺寸")
    private String schoolUniformSize;

    @ApiModelProperty(value = "报读专业")
    private String thinkMajor;

    @ApiModelProperty(value = "报读班型")
    private String classType;

    @ApiModelProperty(value = "文理标志")
    private String studyTag;

    @ApiModelProperty(value = "联系方式")
    private String contactWay;

    @ApiModelProperty(value = "注意事项")
    private String remark;

    @ApiModelProperty(value = "缴费情况")
    private String paymentStatus;

    @ApiModelProperty(value = "头像路径")
    private String headimagePath;

    @ApiModelProperty(value = "性格分析")
    private String characterAnalysis;

    private String regionCode1;

    private String regionCode2;

    private String regionCode3;

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getDormitoryNumber() {
        return dormitoryNumber;
    }

    public void setDormitoryNumber(String dormitoryNumber) {
        this.dormitoryNumber = dormitoryNumber;
    }

    public String getSchoolUniformSize() {
        return schoolUniformSize;
    }

    public void setSchoolUniformSize(String schoolUniformSize) {
        this.schoolUniformSize = schoolUniformSize;
    }

    public String getThinkMajor() {
        return thinkMajor;
    }

    public void setThinkMajor(String thinkMajor) {
        this.thinkMajor = thinkMajor;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getStudyTag() {
        return studyTag;
    }

    public void setStudyTag(String studyTag) {
        this.studyTag = studyTag;
    }

    public String getContactWay() {
        return contactWay;
    }

    public void setContactWay(String contactWay) {
        this.contactWay = contactWay;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getHeadimagePath() {
        return headimagePath;
    }

    public void setHeadimagePath(String headimagePath) {
        this.headimagePath = headimagePath;
    }

    public String getCharacterAnalysis() {
        return characterAnalysis;
    }

    public void setCharacterAnalysis(String characterAnalysis) {
        this.characterAnalysis = characterAnalysis;
    }

    public String getRegionCode1() {
        return regionCode1;
    }

    public void setRegionCode1(String regionCode1) {
        this.regionCode1 = regionCode1;
    }

    public String getRegionCode2() {
        return regionCode2;
    }

    public void setRegionCode2(String regionCode2) {
        this.regionCode2 = regionCode2;
    }

    public String getRegionCode3() {
        return regionCode3;
    }

    public void setRegionCode3(String regionCode3) {
        this.regionCode3 = regionCode3;
    }

    @Override
    public String toString() {
        return "ImStudent{" +
                "fid=" + fid +
                ", realname=" + realname +
                ", gender=" + gender +
                ", school=" + school +
                ", startDate=" + startDate +
                ", number=" + number +
                ", identityNumber=" + identityNumber +
                ", homeAddress=" + homeAddress +
                ", dormitoryNumber=" + dormitoryNumber +
                ", schoolUniformSize=" + schoolUniformSize +
                ", thinkMajor=" + thinkMajor +
                ", classType=" + classType +
                ", studyTag=" + studyTag +
                ", contactWay=" + contactWay +
                ", remark=" + remark +
                ", paymentStatus=" + paymentStatus +
                ", headimagePath=" + headimagePath +
                ", characterAnalysis=" + characterAnalysis +
                "}";
    }

}
