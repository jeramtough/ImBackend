package com.jeramtough.im.model.params;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeramtough.jtweb.model.error.ErrorU;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@ApiModel(value="ImStudentActivity对象", description="")
public class AddOrUpdateStudentActivityParams implements Serializable{

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "fid", type = IdType.AUTO)
    private Long fid;

    @ApiModelProperty(value = "学生id")
    @NotNull(payload = ErrorU.CODE_1.class)
    private Long studentId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "沟通内容")
    private String content;

    @ApiModelProperty(value = "沟通时间")
    private LocalDateTime time;



    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }


    @Override
    public String toString() {
        return "ImStudentActivity{" +
        "fid=" + fid +
        ", studentId=" + studentId +
        ", title=" + title +
        ", content=" + content +
        ", time=" + time +
        "}";
    }
}
