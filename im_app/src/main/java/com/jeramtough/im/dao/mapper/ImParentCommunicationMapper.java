package com.jeramtough.im.dao.mapper;

import com.jeramtough.im.model.dto.ImParentCommunicationDto;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.jtweb.model.QueryPage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
public interface ImParentCommunicationMapper extends BaseMapper<ImParentCommunication> {

    @SelectProvider(value = ImParentCommunicationSqlProvider.class,
            method = "pageByParentCommunicationCondition")
    QueryPage<ImParentCommunicationDto> pageByParentCommunicationCondition
            (QueryPage<ImParentCommunicationDto> queryPage, @Param("keyword") String keyword,
             String permissionFilterSql);

}
