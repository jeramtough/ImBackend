package com.jeramtough.im.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.im.model.dto.ImParentWithStudentDto;
import com.jeramtough.im.model.entity.ImParent;
import com.jeramtough.jtweb.db.mapper.BaseSqlProvider;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-17
 */
public class ImParentSqlProvider extends BaseSqlProvider<ImParent> {

    public String selectAllParentWithStudent(String permissionFilterSql) {
        String sql =
                """
                                SELECT
                                	im_parent.fid AS parent_id,\s
                                	im_parent.`name` AS parent_name,\s
                                	im_parent.relation,\s
                                	im_parent.organization,\s
                                	im_parent.phone_number,\s
                                	im_student.fid AS student_id,\s
                                	im_student.realname AS student_name,\s
                                	im_student.gender AS student_gender,\s
                                	im_student.class_number,\s
                                	im_student.school,\s
                                	im_student.number AS student_number,\s
                                	im_student.identity_number AS student_identity_number
                                FROM
                                	im_parent
                                	INNER JOIN
                                	im_student
                                	ON\s
                                		im_parent.student_id = im_student.fid AND
                                		im_parent.student_id = im_student.fid
                                	WHERE
                        """;
        sql = sql + "\n" + permissionFilterSql;
        return sql;
    }
}
