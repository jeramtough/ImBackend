package com.jeramtough.im.dao.mapper;

import com.jeramtough.im.model.dto.ImParentWithStudentDto;
import com.jeramtough.im.model.entity.ImParent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-17
 */
public interface ImParentMapper extends BaseMapper<ImParent> {

    @SelectProvider(value = ImParentSqlProvider.class, method = "selectAllParentWithStudent")
    List<ImParentWithStudentDto> selectAllParentWithStudent(String permissionFilterSql);
}
