package com.jeramtough.im.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImStudentScore;
import com.jeramtough.jtweb.db.mapper.BaseSqlProvider;
import com.jeramtough.jtweb.model.QueryPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
public class ImStudentScoreSqlProvider extends BaseSqlProvider<ImStudentScore> {

    private static final String ALL_TAG = "全部";

    public String pageByStudentScoreCondition
            (QueryPage<ImStudentScoreDto> queryPage, @Param("classNumber") String classNumber,
             @Param("semester") String semester, @Param("keyword") String keyword,
             String permissionFilterSql) {

        Objects.requireNonNull(permissionFilterSql);

        String sql =
                """
                                SELECT
                                	im_semester.`name` AS semester,\s
                                	im_student.fid AS student_id,\s
                                	im_student.operator_uid AS operator_uid,\s
                                	im_student.realname AS realname,\s
                                	im_student.gender AS gender,\s
                                	im_student.class_number AS class_number,\s
                                	im_student.school AS school,\s
                                	im_student.start_date AS start_date,\s
                                	im_student.number AS number,\s
                                	im_student.dormitory_number AS dormitory_number,\s
                                	im_student.think_major AS think_major,\s
                                	im_student.class_type AS class_type,\s
                                	im_student.study_tag AS study_tag,\s
                                	im_student_score.scores_english,\s
                                	im_student_score.scores_art,\s
                                	im_student_score.fid,\s
                                	im_student_score.scores_comprehensive,\s
                                	im_student_score.scores_chinese,\s
                                	im_student_score.scores_math,\s
                                	im_student_score.semester AS score_semester,\s
                                	im_student_score.create_time,\s
                                	im_student_score.update_time
                                FROM
                                	(
                                		im_semester
                                		JOIN
                                		im_student
                                	)
                                	LEFT JOIN
                                	im_student_score
                                	ON\s
                                		im_student.fid = im_student_score.student_id AND
                                		im_semester.`name` = im_student_score.semester
                                WHERE
                        """;

        sql = sql + "\n" + permissionFilterSql;

        if (StringUtils.hasText(classNumber) && !classNumber.equalsIgnoreCase(ALL_TAG)) {
            sql = sql + "\n AND im_student.class_number = #{classNumber}";
        }
        if (StringUtils.hasText(semester) && !semester.equalsIgnoreCase(ALL_TAG)) {
            sql = sql + "\n AND im_semester.name = #{semester}";
        }
        if (StringUtils.hasText(keyword)) {
            sql = sql + "\n AND (im_student.realname like '%" + keyword + "%' OR im_student" +
                    ".number like '%" + keyword + "%')";
        }

        sql=sql+"\nORDER BY im_student.fid";

        return sql;
    }
}
