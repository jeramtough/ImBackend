package com.jeramtough.im.dao.mapper;

import com.jeramtough.im.model.entity.ImStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-13
 */
public interface ImStudentMapper extends BaseMapper<ImStudent> {

    @Select("SELECT DISTINCT class_number FROM im_student WHERE class_number IS NOT NULL;")
    List<String> selectAllStudentClassNumbers();
}
