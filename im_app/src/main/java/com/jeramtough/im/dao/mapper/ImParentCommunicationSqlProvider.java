package com.jeramtough.im.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.im.model.dto.ImParentCommunicationDto;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.jeramtough.jtweb.db.mapper.BaseSqlProvider;
import com.jeramtough.jtweb.model.QueryPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
public class ImParentCommunicationSqlProvider extends BaseSqlProvider<ImParentCommunication> {

    public String pageByParentCommunicationCondition
            (QueryPage<ImParentCommunicationDto> queryPage, @Param("keyword") String keyword,
             String permissionFilterSql) {

        Objects.requireNonNull(permissionFilterSql);

        String sql =
                """
                                SELECT
                                	`im_parent_communication`.`content` AS `content`,
                                	`im_parent_communication`.`fid` AS `fid`,
                                	`im_parent_communication`.`time` AS `time`,
                                	`im_parent_communication`.`create_time` AS `create_time`,
                                	`im_parent_communication`.`update_time` AS `update_time`,
                                	`im_parent`.`name` AS `parent_name`,
                                	`im_parent`.`phone_number` AS `phone_number`,
                                	`im_parent`.`fid` AS `parent_id`,
                                	`im_student`.`fid` AS `student_id`,
                                	`im_student`.`realname` AS `student_name`,
                                	`im_student`.`number` AS `student_number`,
                                	`im_parent_communication`.`operator_uid` AS `operator_uid`\s
                                FROM
                                	((
                                			`im_parent_communication`
                                			JOIN `im_parent` ON ((
                                					`im_parent_communication`.`parent_id` = `im_parent`.`fid`\s
                                				)))
                                		JOIN `im_student` ON ((
                                			`im_parent`.`student_id` = `im_student`.`fid`\s
                                	)))
                                WHERE
                        """;

        sql = sql + "\n" + permissionFilterSql;

        if (StringUtils.hasText(keyword)) {
            sql = sql + "\n AND (im_student.realname like '%" + keyword + "%' OR im_student" +
                    ".number like '%" + keyword + "%' OR im_parent.name like '%" + keyword + "%'" +
                    " OR im_parent.phone_number='" + keyword + "' OR im_parent_communication" +
                    ".content like '%" + keyword + "%' " +
                    " )";
        }

        sql = sql + "\nORDER BY im_parent_communication.fid DESC";

        return sql;
    }

}
