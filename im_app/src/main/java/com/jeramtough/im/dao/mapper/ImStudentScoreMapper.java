package com.jeramtough.im.dao.mapper;

import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImStudentScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.jtweb.model.QueryPage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
public interface ImStudentScoreMapper extends BaseMapper<ImStudentScore> {


    @SelectProvider(value = ImStudentScoreSqlProvider.class,
            method = "pageByStudentScoreCondition")
    QueryPage<ImStudentScoreDto> pageByStudentScoreCondition
            (QueryPage<ImStudentScoreDto> queryPage,
             @Param("classNumber") String classNumber,
             @Param("semester") String semester, @Param("keyword") String keyword,
             String permissionFilterSql);
}
