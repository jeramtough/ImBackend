package com.jeramtough.im.component.userdetail.login;

import com.alibaba.fastjson.JSON;
import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.jtweb.component.cache.template.CacheTemplate;
import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Created on 2021/8/18 上午9:29
 * by @author WeiBoWen
 * </pre>
 */

@Component
public class UserTokenCacheLoginer extends UserTokenLoginer {

    private final CacheTemplate cacheTemplate;

    @Autowired
    public UserTokenCacheLoginer(AppSetting appSetting,
                                 CacheTemplate cacheTemplate) {
        super(appSetting);
        this.cacheTemplate = cacheTemplate;
    }

    @Override
    public SystemUser login(Object credentials) throws Exception {
        String tokenHeader = (String) credentials;

        String cacheStr = cacheTemplate.get(tokenHeader);
        if (cacheStr != null) {
            SystemUser systemUser = JSON.parseObject(cacheStr, SystemUser.class);
            return systemUser;
        }
        else {
            SystemUser systemUser = super.login(credentials);
            try {
                String cacheJson = JSON.toJSONString(systemUser);
                cacheTemplate.put(cacheJson, cacheJson);
            }
            catch (Exception ignored) {
            }
            return systemUser;
        }
    }
}
