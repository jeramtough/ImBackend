package com.jeramtough.im.component.userdetail;

import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;

/**
 * <pre>
 * Created on 2020/1/29 23:49
 * by @author JeramTough
 * </pre>
 */
public class UserHolder {

    private static SystemUser systemUser;

    public static boolean hasLogined() {
        if ((UserHolder.systemUser != null)) {
            return true;
        }
        return false;
    }

    public static SystemUser getSystemUser() {
        return UserHolder.systemUser;
    }


    public static void afterLogin(SystemUser systemUser) {
        UserHolder.systemUser = systemUser;
    }


}
