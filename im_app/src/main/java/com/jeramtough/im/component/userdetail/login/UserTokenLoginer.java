package com.jeramtough.im.component.userdetail.login;

import com.alibaba.fastjson.JSON;
import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.jtweb.component.apiresponse.error.ErrorCode;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiException;
import com.jeramtough.randl2.sdk.api.DefaultRandlApi;
import com.jeramtough.randl2.sdk.api.RandlApi;
import com.jeramtough.randl2.sdk.api.ResourceApiUrl;
import com.jeramtough.randl2.sdk.model.httpresponse.ApiResponse;
import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Created on 2021/8/18 上午9:29
 * by @author WeiBoWen
 * </pre>
 */

@Component
public class UserTokenLoginer implements UserLoginer {

    private final AppSetting appSetting;

    @Autowired
    public UserTokenLoginer(AppSetting appSetting) {
        this.appSetting = appSetting;
    }

    @Override
    public SystemUser login(Object credentials) throws Exception {
        String tokenHeader = (String) credentials;

        RandlApi randlApi = new DefaultRandlApi(
                appSetting.getRandlResourceApiContextUrl());
        randlApi.setTokenHeader(tokenHeader);

        ApiResponse apiResponse = randlApi.doPost(ResourceApiUrl.USER_INFO, null);

        if (apiResponse.isSuccessful()) {
            String resultJson = apiResponse.getResponseBody().toJSONString();
            SystemUser systemUser = JSON.parseObject(resultJson, SystemUser.class);
            return systemUser;
        }
        else {
            throw new ApiException(ErrorCode.U, apiResponse.getMessage());
        }
    }
}
