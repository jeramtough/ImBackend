package com.jeramtough.im.component.userdetail.permission.queryfilter;

import cn.hutool.core.util.ClassUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.randl2.sdk.model.httpresponse.RandlRole;
import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <pre>
 * Created on 2021/9/18 上午9:42
 * by @author WeiBoWen
 * </pre>
 */

public abstract class BaseQueryPermissionFilter<T> implements QueryPermissionFilter<T> {

    private final AppSetting appSetting;

    public BaseQueryPermissionFilter(AppSetting appSetting) {
        this.appSetting = appSetting;
    }

    @Override
    public QueryWrapper<T> doFilter(QueryWrapper<T> queryWrapper) {
        SystemUser systemUser = UserHolder.getSystemUser();
        boolean hasAdministratorRoleId = hasAdministratorRoleId();

        //拥有超级管理员角色id直接返回
        if (hasAdministratorRoleId) {
            return queryWrapper;
        }
        else {
            queryWrapper.eq(getTableName() + ".operator_uid", systemUser.getUid());
            return queryWrapper;
        }

    }

    @Override
    public String getFilterSql() {
        return getFilterSql(getTableName());
    }

    @Override
    public String getFilterSql(String tableName) {
        SystemUser systemUser = UserHolder.getSystemUser();
        boolean hasAdministratorRoleId = hasAdministratorRoleId();

        //拥有超级管理员角色id直接返回
        if (hasAdministratorRoleId) {
            return "1=1";
        }
        else {
            return tableName + ".operator_uid = " + systemUser.getUid();
        }
    }

    public String getTableName() {
        Class<T> entityClass = (Class<T>) ClassUtil.getTypeArgument(getClass(),
                0);
        Objects.requireNonNull(entityClass);
        return com.jeramtough.jtcomponent.utils.StringUtil.humpToLine(
                entityClass.getSimpleName());
    }

    //*********************

    private boolean hasAdministratorRoleId() {
        SystemUser systemUser = UserHolder.getSystemUser();
        Objects.requireNonNull(systemUser);

        List<RandlRole> roleList = systemUser.getRoles();
        Set<Long> roleIds = roleList.parallelStream().map(RandlRole::getFid).collect(
                Collectors.toSet());

        Long administratorRoleId = appSetting.getAdministratorSetting().getRoldId();

        //拥有超级管理员角色id直接返回
        if (roleIds.contains(administratorRoleId)) {
            return true;
        }
        else {
            return false;
        }
    }

}
