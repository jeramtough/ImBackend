package com.jeramtough.im.component.timesheet;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImStudentTimeSheet;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * Created on 2021/9/25 下午8:07
 * by @author WeiBoWen
 * </pre>
 */
public class DefaultTimeSheetComposer implements TimeSheetComposer {

    private final String EMPTY_TAG = "[/]";

    private final String dateFormat;
    private final List<Date> allDayOfMonth;
    private final Map<String, ImStudentTimeSheet> timeAndIdKeyStudentTimeSheetMap;

    public DefaultTimeSheetComposer(String dateFormat, List<Date> allDayOfMonth,
                                    Map<String, ImStudentTimeSheet> timeAndIdKeyStudentTimeSheetMap) {
        this.dateFormat = dateFormat;
        this.allDayOfMonth = allDayOfMonth;
        this.timeAndIdKeyStudentTimeSheetMap = timeAndIdKeyStudentTimeSheetMap;
    }

    @Override
    public Map<String, Object> compose(ImStudentDto imStudentDto) {
        JSONObject timeSheet = JSON.parseObject(JSON.toJSONString(imStudentDto));
        timeSheet.remove("fid");
        timeSheet.put("studentId", imStudentDto.getFid());
        timeSheet.remove("number");
        timeSheet.put("studentNumber", imStudentDto.getNumber());
        timeSheet.remove("realname");
        timeSheet.put("studentName", imStudentDto.getRealname());
        for (Date dayOfMonthEach : allDayOfMonth) {
            String key = DateUtil.format(dayOfMonthEach, dateFormat);
            key = key + ":" + imStudentDto.getFid();
            ImStudentTimeSheet imStudentTimeSheet = timeAndIdKeyStudentTimeSheetMap.get(
                    key);
            String dayKey = "day" + DateUtil.format(dayOfMonthEach, "dd");

            if (imStudentTimeSheet == null) {
                timeSheet.put(dayKey, EMPTY_TAG + "," + EMPTY_TAG + "," + EMPTY_TAG);
            }
            else {
                String value = "";
                if (StringUtils.hasText(imStudentTimeSheet.getMorning())) {
                    value = imStudentTimeSheet.getMorning();
                }
                else {
                    value = value + EMPTY_TAG;
                }

                if (StringUtils.hasText(imStudentTimeSheet.getAfternoon())) {
                    value = "\n" + imStudentTimeSheet.getAfternoon();
                }
                else {
                    value = value + "," + EMPTY_TAG;
                }

                if (StringUtils.hasText(imStudentTimeSheet.getEvening())) {
                    value = "\n" + imStudentTimeSheet.getEvening();
                }
                else {
                    value = value + "," + EMPTY_TAG;
                }
                timeSheet.put(dayKey, value);
            }
        }
        return timeSheet;
    }
}
