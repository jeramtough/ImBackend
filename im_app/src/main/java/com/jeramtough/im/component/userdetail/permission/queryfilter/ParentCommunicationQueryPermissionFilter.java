package com.jeramtough.im.component.userdetail.permission.queryfilter;

import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.jeramtough.im.model.entity.ImStudentScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Created on 2021/9/18 上午9:52
 * by @author WeiBoWen
 * </pre>
 */
@Component
public class ParentCommunicationQueryPermissionFilter
        extends BaseQueryPermissionFilter<ImParentCommunication> {

    @Autowired
    public ParentCommunicationQueryPermissionFilter(AppSetting appSetting) {
        super(appSetting);
    }
}
