package com.jeramtough.im.component.timesheet;

import com.jeramtough.im.model.dto.ImStudentDto;

import java.util.Map;

/**
 * <pre>
 * Created on 2021/9/25 下午8:06
 * by @author WeiBoWen
 * </pre>
 */
public interface TimeSheetComposer {

    Map<String, Object> compose(ImStudentDto imStudentDto);

}
