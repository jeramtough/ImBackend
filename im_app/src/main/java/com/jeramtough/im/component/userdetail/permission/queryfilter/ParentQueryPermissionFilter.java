package com.jeramtough.im.component.userdetail.permission.queryfilter;

import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.im.model.entity.ImParent;
import com.jeramtough.im.model.entity.ImStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Created on 2021/9/18 上午9:52
 * by @author WeiBoWen
 * </pre>
 */
@Component
public class ParentQueryPermissionFilter extends BaseQueryPermissionFilter<ImParent> {

    @Autowired
    public ParentQueryPermissionFilter(AppSetting appSetting) {
        super(appSetting);
    }
}
