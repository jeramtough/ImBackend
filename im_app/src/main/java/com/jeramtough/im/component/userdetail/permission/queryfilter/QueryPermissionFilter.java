package com.jeramtough.im.component.userdetail.permission.queryfilter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <pre>
 * Created on 2021/9/18 上午9:38
 * by @author WeiBoWen
 * </pre>
 */
public interface QueryPermissionFilter<T> {

    QueryWrapper<T> doFilter(QueryWrapper<T> queryWrapper);

    String getFilterSql();

    String getFilterSql(String tableName);

}
