package com.jeramtough.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.model.dto.ImStudentActivityDto;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.jeramtough.im.model.entity.ImStudent;
import com.jeramtough.im.model.entity.ImStudentActivity;
import com.jeramtough.im.dao.mapper.ImStudentActivityMapper;
import com.jeramtough.im.model.params.AddOrUpdateStudentActivityParams;
import com.jeramtough.im.service.ImStudentActivityService;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@Service
public class ImStudentActivityServiceImpl extends MyBaseServiceImpl<ImStudentActivityMapper,
        ImStudentActivity, ImStudentActivityDto> implements ImStudentActivityService {

    private final ImStudentService imStudentService;

    @Autowired
    public ImStudentActivityServiceImpl(WebApplicationContext wc,
                                        ImStudentService imStudentService) {
        super(wc);
        this.imStudentService = imStudentService;
    }

    @Override
    protected ImStudentActivityDto toDto(ImStudentActivity imStudentActivity) {

        ImStudentActivityDto dto = getMapperFacade().map(imStudentActivity,
                ImStudentActivityDto.class);
        ImStudent imStudent = imStudentService.getById(imStudentActivity.getStudentId());

        Objects.requireNonNull(imStudent);

        dto.setStudentNumber(imStudent.getNumber());
        dto.setStudentName(imStudent.getRealname());

        return dto;
    }

    @Override
    public String addSudentActivity(AddOrUpdateStudentActivityParams params) {
        BeanValidator.verifyParams(params);

        ImStudentActivity entity = getMapperFacade().map(params,
                ImStudentActivity.class);
        Long operatorUid = UserHolder.getSystemUser().getUid();
        entity.setOperatorUid(operatorUid);

        return addByParams(entity);
    }

    @Override
    public String updateSudentActivity(AddOrUpdateStudentActivityParams params) {
        return updateByParams(params);
    }

    @Override
    public String getActivityContentById(Long fid) {
        ImStudentActivity entity = getById(fid);
        return entity.getContent();
    }

    @Override
    public PageDto<ImStudentActivityDto> pageByConditionTwo(
            QueryByPageParams queryByPageParams, BaseConditionParams params,
            QueryWrapper<ImStudentActivity> queryWrapper) {

        queryWrapper.select("fid", "student_id", "title", "time", "create_time",
                "update_time");

        return super.pageByConditionTwo(queryByPageParams, params, queryWrapper);
    }
}
