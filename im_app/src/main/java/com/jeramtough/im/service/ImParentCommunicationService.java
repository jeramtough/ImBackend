package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImParentCommunicationDto;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.jeramtough.im.model.params.AddOrUpdateParentCommunicationParams;
import com.jeramtough.im.model.params.ParentCommunicationConditionParams;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.im.service.base.MyBaseService;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
public interface ImParentCommunicationService extends MyBaseService<ImParentCommunication,
        ImParentCommunicationDto> {

    String addImParentCommunication(AddOrUpdateParentCommunicationParams params);

    PageDto<ImParentCommunicationDto> pageByParentCommunicationCondition(
            QueryByPageParams queryByPageParams,
            ParentCommunicationConditionParams conditionParams);

}
