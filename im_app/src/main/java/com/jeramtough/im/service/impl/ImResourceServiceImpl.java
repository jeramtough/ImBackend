package com.jeramtough.im.service.impl;

import cn.hutool.core.io.IoUtil;
import com.alibaba.fastjson.JSON;
import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.im.config.setting.Headimage;
import com.jeramtough.im.model.MyErrorU;
import com.jeramtough.im.service.ImResourceService;
import com.jeramtough.jtcomponent.utils.Base64Util;
import com.jeramtough.jtlog.facade.L;
import com.jeramtough.jtlog.message.MessageWrapper;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseException;
import com.jeramtough.jtweb.component.filesaver.core.FileSaver;
import com.jeramtough.jtweb.component.filesaver.core.ImageFileSaver;
import com.jeramtough.jtweb.component.filesaver.exception.IllegalFileTypeException;
import com.jeramtough.jtweb.component.filesaver.exception.MaxSizeLimitException;
import com.jeramtough.jtweb.component.filesaver.exception.SaveFileException;
import com.jeramtough.jtweb.model.error.ErrorS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <pre>
 * Created on 2021/9/15 上午10:40
 * by @author WeiBoWen
 * </pre>
 */
@Service
public class ImResourceServiceImpl implements ImResourceService {

    private final AppSetting appSetting;


    @Autowired
    public ImResourceServiceImpl(AppSetting appSetting) {
        this.appSetting = appSetting;


    }

    @Override
    public Map<String, Object> uploadSurfaceImage(MultipartFile file) {
        Headimage headimageConfig = appSetting.getUploadFileSetting().getHeadimage();
        FileSaver fileSaver = new ImageFileSaver(headimageConfig);

        File imageFile;
        try {
            imageFile = fileSaver.save(file);
        }
        catch (IOException e) {
            throw new ApiResponseException(ErrorS.CODE_7.C, "上传", e.getMessage());
        }
        catch (MaxSizeLimitException e) {
            throw new ApiResponseException(MyErrorU.CODE_602.C,
                    headimageConfig.getMaxSize() + "kb");
        }
        catch (IllegalFileTypeException e) {
            throw new ApiResponseException(MyErrorU.CODE_603.C, headimageConfig.getType());
        }
        catch (SaveFileException e) {
            throw new ApiResponseException(ErrorS.CODE_2.C, "上传");
        }

        Objects.requireNonNull(imageFile);


        Map<String, Object> resultMap = new HashMap<>(3);
        resultMap.put("path", fileSaver.getRelativePath());
        L.debug(() -> JSON.toJSONString(resultMap));
        return resultMap;
    }

    @Override
    public void readSurfaceImage(HttpServletResponse response, String imagePath) {
        Headimage headimageConfig = appSetting.getUploadFileSetting().getHeadimage();
        FileSaver fileSaver = new ImageFileSaver(headimageConfig);

        File imageFile = getHeadimage(fileSaver, imagePath);

        response.setContentType(fileSaver.getFileContentType(imageFile));

        try {
            IoUtil.copy(new FileInputStream(imageFile), response.getOutputStream());
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new ApiResponseException(ErrorS.CODE_2.C, "读取图片资源");
        }
    }

    @Override
    public boolean deleteHeadimageByPath(String imagePath) {
        Headimage headimageConfig = appSetting.getUploadFileSetting().getHeadimage();
        FileSaver fileSaver = new ImageFileSaver(headimageConfig);

        File imageFile;
        try {
            imageFile = fileSaver.read(imagePath);
        }
        catch (NoSuchFileException e) {
            return false;
        }

        return imageFile.delete();
    }

    @Override
    public String readSurfaceImageForBase64(String imagePath) {
        Headimage headimageConfig = appSetting.getUploadFileSetting().getHeadimage();
        FileSaver fileSaver = new ImageFileSaver(headimageConfig);

        File imageFile = getHeadimage(fileSaver, imagePath);

        try {
            String base64 = Base64Util.toBase64Str(
                    IoUtil.readBytes(new FileInputStream(imageFile)));
            return base64;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new ApiResponseException(ErrorS.CODE_2.C, "读取base64图片资源失败！");
        }
    }

    //**************


    private File getHeadimage(FileSaver fileSaver, String imagePath) {
        try {
            File imageFile = fileSaver.read(imagePath);
            return imageFile;
        }
        catch (NoSuchFileException e) {
            throw new ApiResponseException(ErrorS.CODE_7.C, "读取图片",
                    e.getMessage() + "图片文件不存在");
        }
    }

}
