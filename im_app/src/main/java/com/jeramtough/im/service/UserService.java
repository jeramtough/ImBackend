package com.jeramtough.im.service;

import java.util.Map;

/**
 * <pre>
 * Created on 2021/8/17 下午10:58
 * by @author WeiBoWen
 * </pre>
 */
public interface UserService {

    Map<String, Object> getInfo();
}
