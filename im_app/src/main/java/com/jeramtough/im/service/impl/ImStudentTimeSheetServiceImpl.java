package com.jeramtough.im.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeramtough.im.component.timesheet.DefaultTimeSheetComposer;
import com.jeramtough.im.component.timesheet.TimeSheetComposer;
import com.jeramtough.im.component.userdetail.permission.queryfilter.StudentQueryPermissionFilter;
import com.jeramtough.im.component.userdetail.permission.queryfilter.StudentTimeSheetQueryPermissionFilter;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.dto.ImStudentTimeSheetDto;
import com.jeramtough.im.model.entity.ImStudentTimeSheet;
import com.jeramtough.im.dao.mapper.ImStudentTimeSheetMapper;
import com.jeramtough.im.model.params.AddOrUpdateImStudentTimeSheetParams;
import com.jeramtough.im.model.params.StudentTimeSheetConditionParams;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.ImStudentTimeSheetService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.im.util.MyDateTimeUtil;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@Service
public class ImStudentTimeSheetServiceImpl
        extends MyBaseServiceImpl<ImStudentTimeSheetMapper, ImStudentTimeSheet,
        ImStudentTimeSheetDto>
        implements ImStudentTimeSheetService {

    private final ImStudentService imStudentService;

    @Autowired
    public ImStudentTimeSheetServiceImpl(
            WebApplicationContext wc,
            ImStudentService imStudentService) {
        super(wc);
        this.imStudentService = imStudentService;
    }

    @Override
    public String updateStudentTimeSheet(List<AddOrUpdateImStudentTimeSheetParams> params) {
        return null;
    }

    @Override
    public List<String> getDayKeys(Date date) {
        //当月所有日期
        List<Date> allDayOfMonth = MyDateTimeUtil.getDaysOfMonth(date);
        List<String> dayKeyList = allDayOfMonth
                .stream()
                .map(date1 -> {
                    String dayOfMonth = DateUtil.format(date1, "dd");
                    return "day" + dayOfMonth;
                })
                .collect(Collectors.toList());

        return dayKeyList;
    }

    @Override
    public PageDto<Map<String, Object>> pageByStudentTimeSheetCondition(
            QueryByPageParams queryByPageParams,
            StudentTimeSheetConditionParams conditionParams) {

        BeanValidator.verifyParams(conditionParams);
        String dateFormat = "yyyy-MM-dd";

        //先把需要查询的学生列表查出来
        conditionParams.setOrderBy("fid");
        conditionParams.setIsAsc(true);
        PageDto<ImStudentDto> studentPageDto =
                imStudentService.pageByCondition(queryByPageParams, conditionParams);
        List<Long> studentIds = imStudentService.getStudentIdsByPageDto(studentPageDto);

        //符合范围内的学生考勤记录查出
        List<ImStudentTimeSheet> studentTimeSheetList = getImStudentTimeSheetsByStudentIdAndDate(
                conditionParams.getDate(), studentIds);

        //当月所有日期
        List<Date> allDayOfMonth = MyDateTimeUtil.getDaysOfMonth(conditionParams.getDate());


        //考勤日期和学生id为key
        Map<String, ImStudentTimeSheet> timeAndIdKeyStudentTimeSheetMap =
                studentTimeSheetList.parallelStream()
                                    .collect(Collectors.toMap(
                                            imStudentTimeSheet -> LocalDateTimeUtil.format(
                                                    imStudentTimeSheet.getDate(),
                                                    dateFormat) + ":" + imStudentTimeSheet.getStudentId(),
                                            imStudentTimeSheet -> imStudentTimeSheet));

        //组装返回到前端的考勤实体
        List<Map<String, Object>> timeSheetList = new ArrayList<>();
        for (ImStudentDto imStudentDto : studentPageDto.getList()) {
            TimeSheetComposer timeSheetComposer = new DefaultTimeSheetComposer(dateFormat,
                    allDayOfMonth, timeAndIdKeyStudentTimeSheetMap);
            Map<String, Object> timeSheet = timeSheetComposer.compose(imStudentDto);
            timeSheetList.add(timeSheet);
        }

        //组装结果
        PageDto<Map<String, Object>> pageDto = new PageDto<>();
        pageDto.setIndex(studentPageDto.getIndex());
        pageDto.setSize(studentPageDto.getSize());
        pageDto.setTotal(studentPageDto.getTotal());
        pageDto.setList(timeSheetList);
        return pageDto;
    }

    @Override
    public List<ImStudentTimeSheet> getImStudentTimeSheetsByStudentIdAndDate(
            Date date, List<Long> studentIds) {
        QueryWrapper<ImStudentTimeSheet> queryWrapper = new QueryWrapper<>();
        Date startDate = DateUtil.beginOfMonth(date);
        Date endDate = DateUtil.endOfMonth(date);
        queryWrapper.and(wrapper ->
                wrapper.between("date", startDate,
                        endDate));
        queryWrapper.in("student_id", studentIds);

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentTimeSheetQueryPermissionFilter.class).doFilter(
                        queryWrapper);
        List<ImStudentTimeSheet> studentTimeSheetList = list(queryWrapper);
        return studentTimeSheetList;
    }


}
