package com.jeramtough.im.service.impl;

import com.alibaba.fastjson.JSON;
import com.jeramtough.im.config.setting.AppSetting;
import com.jeramtough.im.model.params.UserCredentials;
import com.jeramtough.im.service.LoginService;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseException;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.error.ErrorS;
import com.jeramtough.randl2.sdk.client.Oauth2PasswordGrantTypeHttpClient;
import com.jeramtough.randl2.sdk.exception.OptionException;
import com.jeramtough.randl2.sdk.model.httpresponse.TokenBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * <pre>
 * Created on 2021/8/17 下午10:04
 * by @author WeiBoWen
 * </pre>
 */
@Service
public class LoginServiceImpl implements LoginService {

    private final AppSetting appSetting;

    @Autowired
    public LoginServiceImpl(AppSetting appSetting) {
        this.appSetting = appSetting;
    }

    @Override
    public Map<String, Object> login(UserCredentials userCredentials) {
        BeanValidator.verifyParams(userCredentials);

        Oauth2PasswordGrantTypeHttpClient oauth2PasswordGrantTypeHttpClient =
                new Oauth2PasswordGrantTypeHttpClient(appSetting.getOauth2ClientConfig());
        try {
            TokenBody tokenBody = oauth2PasswordGrantTypeHttpClient.obtainTokenByPassword(
                    userCredentials.getUsername(),
                    userCredentials.getPassword());
            Map<String, Object> result = JSON.parseObject(JSON.toJSONString(tokenBody));
            return result;
        }
        catch (Exception e) {
            if (!(e instanceof OptionException)) {
                e.printStackTrace();
            }
            throw new ApiResponseException(ErrorS.CODE_2.C, e.getMessage());
        }
    }
}
