package com.jeramtough.im.service.impl;

import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.component.userdetail.permission.queryfilter.ParentCommunicationQueryPermissionFilter;
import com.jeramtough.im.component.userdetail.permission.queryfilter.StudentScoreQueryPermissionFilter;
import com.jeramtough.im.model.dto.ImParentCommunicationDto;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImParentCommunication;
import com.jeramtough.im.dao.mapper.ImParentCommunicationMapper;
import com.jeramtough.im.model.entity.ImStudent;
import com.jeramtough.im.model.params.AddOrUpdateParentCommunicationParams;
import com.jeramtough.im.model.params.ParentCommunicationConditionParams;
import com.jeramtough.im.service.ImParentCommunicationService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.QueryPage;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
@Service
public class ImParentCommunicationServiceImpl
        extends MyBaseServiceImpl<ImParentCommunicationMapper,
        ImParentCommunication, ImParentCommunicationDto>
        implements ImParentCommunicationService {

    @Autowired
    public ImParentCommunicationServiceImpl(
            WebApplicationContext wc) {
        super(wc);
    }

    @Override
    public String addImParentCommunication(AddOrUpdateParentCommunicationParams params) {
        BeanValidator.verifyParams(params);

        ImParentCommunication entity = getMapperFacade().map(params,
                ImParentCommunication.class);
        Long operatorUid = UserHolder.getSystemUser().getUid();
        entity.setOperatorUid(operatorUid);

        return addByParams(entity);
    }

    @Override
    public PageDto<ImParentCommunicationDto> pageByParentCommunicationCondition(
            QueryByPageParams queryByPageParams,
            ParentCommunicationConditionParams conditionParams) {

        QueryPage<ImParentCommunicationDto> queryPage = new QueryPage<>(queryByPageParams);

        //因为主要用的是学生表的来过滤
        String permissionFilterSql =
                getWC().getBean(ParentCommunicationQueryPermissionFilter.class)
                       .getFilterSql();

        queryPage =
                getBaseMapper().pageByParentCommunicationCondition(queryPage,
                        conditionParams.getKeyword(),
                        permissionFilterSql);

        PageDto<ImParentCommunicationDto> pageDto = new PageDto<>();
        pageDto.setIndex(queryPage.getCurrent());
        pageDto.setSize(queryPage.getSize());
        pageDto.setTotal(queryPage.getTotal());
        pageDto.setList(queryPage.getRecords());
        return pageDto;
    }
}
