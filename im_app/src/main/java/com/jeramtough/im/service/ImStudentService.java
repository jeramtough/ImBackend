package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImStudent;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.service.base.MyBaseService;
import com.jeramtough.jtweb.model.dto.PageDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiJieHui
 * @since 2021-08-24
 */
public interface ImStudentService extends MyBaseService<ImStudent, ImStudentDto> {

    ImStudentDto getByNumber(String studentNumber);

    /**
     * 查询结果大于1个就会抛出不合法异常
     */
    ImStudentDto getByNumberOrName(String studentNumber) throws IllegalStateException;

    Map<String, Object> uploadSurfaceImage(MultipartFile file);

    String addStudent(AddOrUpdateStudentParams params);

    String updateStudent(AddOrUpdateStudentParams params);

    List<ImStudentDto> getAll();

    List<ImStudentDto> getListByKeyword(String keyword);

    List<String> getStudentClassNumbers();

    List<Long> getStudentIdsByPageDto(PageDto<ImStudentDto> studentPageDto);
}
