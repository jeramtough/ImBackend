package com.jeramtough.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.component.userdetail.permission.queryfilter.StudentQueryPermissionFilter;
import com.jeramtough.im.dao.mapper.ImStudentMapper;
import com.jeramtough.im.model.MyErrorU;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImStudent;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.model.params.StudentConditionParams;
import com.jeramtough.im.service.ImResourceService;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseBeanException;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseException;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.error.ErrorU;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author WeiJieHui
 * @since 2021-08-24
 */
@Service
public class ImStudentServiceImpl
        extends MyBaseServiceImpl<ImStudentMapper, ImStudent, ImStudentDto>
        implements ImStudentService {

    private final ImResourceService imResourceService;

    @Autowired
    public ImStudentServiceImpl(WebApplicationContext wc,
                                ImResourceService imResourceService) {
        super(wc);
        this.imResourceService = imResourceService;
    }

    @Override
    public PageDto<ImStudentDto> pageByConditionTwo(QueryByPageParams queryByPageParams,
                                                    BaseConditionParams params,
                                                    QueryWrapper<ImStudent> queryWrapper) {
        if (StringUtils.hasText(params.getKeyword())) {
            getKeywordQueryWrapper(queryWrapper, params.getKeyword());
        }

        if (params instanceof StudentConditionParams studentConditionParams) {
            if (StringUtils.hasText(studentConditionParams.getClassNumber())) {
                queryWrapper.eq("class_number", studentConditionParams.getClassNumber());
            }
        }

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentQueryPermissionFilter.class).doFilter(queryWrapper);

        return super.pageByConditionTwo(queryByPageParams, params, queryWrapper);
    }

    @Override
    public ImStudentDto getByNumber(String studentNumber) {
        QueryWrapper<ImStudent> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("number", studentNumber);

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentQueryPermissionFilter.class).doFilter(queryWrapper);

        ImStudent imStudent = getOne(queryWrapper);

        return toDto(imStudent);
    }

    @Override
    public ImStudentDto getByNumberOrName(String studentNumberOrName) throws
            IllegalStateException {

        QueryWrapper<ImStudent> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("number", studentNumberOrName).or()
                    .eq("realname", studentNumberOrName);

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentQueryPermissionFilter.class).doFilter(queryWrapper);

        List<ImStudent> imStudentList = list(queryWrapper);
        if (imStudentList.size() > 1) {
            throw new IllegalStateException("存在重名结果");
        }

        return toDto(imStudentList.get(0));
    }

    @Override
    public Map<String, Object> uploadSurfaceImage(MultipartFile file) {

        if (file.isEmpty()) {
            throw new ApiResponseException(MyErrorU.CODE_601.C);
        }

        return null;
    }

    @Override
    public String addStudent(AddOrUpdateStudentParams params) {
        BeanValidator.verifyParams(params);

        if (count(new QueryWrapper<ImStudent>().eq("number", params.getNumber())) > 0) {
            throw new ApiResponseBeanException(ErrorU.CODE_10.C, "学号");
        }

        if (count(new QueryWrapper<ImStudent>().eq("identity_number",
                params.getIdentityNumber())) > 0) {
            throw new ApiResponseBeanException(ErrorU.CODE_10.C, "身份证号");
        }


        ImStudent imStudent = getMapperFacade().map(params, ImStudent.class);
        Long operatorUid = UserHolder.getSystemUser().getUid();
        imStudent.setOperatorUid(operatorUid);

        return addByParams(imStudent);
    }

    @Override
    public String updateStudent(AddOrUpdateStudentParams params) {
        BeanValidator.verifyParams(params);

        Objects.requireNonNull(params.getFid());

        ImStudent oldStudent = getById(params.getFid());
        if (params.getNumber() != null && !params.getNumber().equals(oldStudent.getNumber())) {
            if (count(new QueryWrapper<ImStudent>().eq("number", params.getNumber())) > 0) {
                throw new ApiResponseBeanException(ErrorU.CODE_10.C, "学号");
            }
        }

        if (params.getIdentityNumber() != null && !params.getIdentityNumber().equals(
                oldStudent.getIdentityNumber())) {
            if (count(new QueryWrapper<ImStudent>().eq("identity_number",
                    params.getIdentityNumber())) > 0) {
                throw new ApiResponseBeanException(ErrorU.CODE_10.C, "身份证号");
            }
        }

        if (params.getHeadimagePath() != null && !params.getHeadimagePath().equals(
                oldStudent.getHeadimagePath())) {
            String result = updateByParams(params);

            //删除旧的头像
            getBaseExecutorService().execute(
                    () -> imResourceService.deleteHeadimageByPath(
                            oldStudent.getHeadimagePath()));
            return result;
        }

        return updateByParams(params);
    }

    @Override
    public List<ImStudentDto> getAll() {
        QueryWrapper<ImStudent> queryWrapper = new QueryWrapper<>();

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentQueryPermissionFilter.class).doFilter(queryWrapper);
        List<ImStudent> list = list(queryWrapper);

        return getDtoList(list);
    }

    @Override
    public List<ImStudentDto> getListByKeyword(String keyword) {
        QueryWrapper<ImStudent> queryWrapper = new QueryWrapper<>();

        getKeywordQueryWrapper(queryWrapper, keyword);

        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(StudentQueryPermissionFilter.class).doFilter(queryWrapper);

        return getDtoListForAsyn(list(queryWrapper));
    }

    @Override
    public List<String> getStudentClassNumbers() {
        return getBaseMapper().selectAllStudentClassNumbers();
    }

    @Override
    public List<Long> getStudentIdsByPageDto(PageDto<ImStudentDto> studentPageDto) {
        return studentPageDto.getList().parallelStream().map(ImStudentDto::getFid).collect(
                Collectors.toList());
    }


    //*******************************************************8

    private void getKeywordQueryWrapper(
            QueryWrapper<ImStudent> queryWrapper,
            String keyword) {
        queryWrapper.like("realname", keyword)
                    .or().like("number", keyword)
                    .or().like("identity_number", keyword);
    }


}
