package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImStudentActivityDto;
import com.jeramtough.im.model.entity.ImStudentActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeramtough.im.model.params.AddOrUpdateParentParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentActivityParams;
import com.jeramtough.im.service.base.MyBaseService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
public interface ImStudentActivityService extends MyBaseService<ImStudentActivity,
        ImStudentActivityDto> {

    String addSudentActivity(AddOrUpdateStudentActivityParams params);

    String updateSudentActivity(AddOrUpdateStudentActivityParams params);

    String getActivityContentById(Long fid);
}
