package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImParentDto;
import com.jeramtough.im.model.dto.ImParentWithStudentDto;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImParent;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeramtough.im.model.params.AddOrUpdateParentParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.service.base.MyBaseService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-17
 */
public interface ImParentService extends MyBaseService<ImParent, ImParentDto> {

    String addParent(AddOrUpdateParentParams params);

    String updateParent(AddOrUpdateParentParams params);

    List<ImParentWithStudentDto> getAll();

}
