package com.jeramtough.im.service.base.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeramtough.im.service.base.MyBaseService;
import com.jeramtough.jtweb.service.impl.JtBaseServiceImpl;
import org.springframework.web.context.WebApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <pre>
 * Created on 2020/10/25 16:43
 * by @author WeiBoWen
 * </pre>
 */
public abstract class MyBaseServiceImpl<M extends BaseMapper<T>, T, D>
        extends JtBaseServiceImpl<M, T, D> implements MyBaseService<T, D> {


    private volatile ExecutorService executorService;


    public MyBaseServiceImpl(WebApplicationContext wc) {
        super(wc);
    }

    public ExecutorService getBaseExecutorService() {
        if (executorService == null) {
            synchronized (MyBaseServiceImpl.class) {
                if (executorService == null) {
                    executorService = Executors.newCachedThreadPool();
                }
            }
        }
        return executorService;
    }
}
