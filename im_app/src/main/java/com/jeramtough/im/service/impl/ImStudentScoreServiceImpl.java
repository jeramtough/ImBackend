package com.jeramtough.im.service.impl;

import com.jeramtough.im.component.userdetail.permission.queryfilter.StudentScoreQueryPermissionFilter;
import com.jeramtough.im.dao.mapper.ImStudentScoreMapper;
import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImStudentScore;
import com.jeramtough.im.model.params.AddOrUpdateStudentScoreParams;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.im.service.ImStudentScoreService;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.jtcomponent.utils.StringUtil;
import com.jeramtough.jtweb.model.QueryPage;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
@Service
public class ImStudentScoreServiceImpl extends MyBaseServiceImpl<ImStudentScoreMapper,
        ImStudentScore, ImStudentScoreDto>
        implements ImStudentScoreService {

    private final ImStudentService imStudentService;

    @Autowired
    public ImStudentScoreServiceImpl(WebApplicationContext wc,
                                     ImStudentService imStudentService) {
        super(wc);
        this.imStudentService = imStudentService;
    }

    @Override
    public String updateStudentScore(List<AddOrUpdateStudentScoreParams> params) {
        return addOrUpdateBatchByParamsList(params);
    }

    @Override
    public PageDto<ImStudentScoreDto> pageByStudentScoreCondition(
            QueryByPageParams queryByPageParams, StudentScoreConditionParams conditionParams) {

        QueryPage<ImStudentScoreDto> queryPage = new QueryPage<>(queryByPageParams);

        //因为主要用的是学生表的来过滤
        String permissionFilterSql =
                getWC().getBean(StudentScoreQueryPermissionFilter.class).getFilterSql(
                        "im_student");

        queryPage =
                getBaseMapper().pageByStudentScoreCondition(queryPage,
                        conditionParams.getClassNumber(),
                        conditionParams.getSemester(),
                        conditionParams.getKeyword(),
                        permissionFilterSql);

        PageDto<ImStudentScoreDto> pageDto = new PageDto<>();
        pageDto.setIndex(queryPage.getCurrent());
        pageDto.setSize(queryPage.getSize());
        pageDto.setTotal(queryPage.getTotal());
        pageDto.setList(queryPage.getRecords());
        return pageDto;
    }

}
