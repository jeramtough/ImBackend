package com.jeramtough.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.component.userdetail.permission.queryfilter.ParentQueryPermissionFilter;
import com.jeramtough.im.dao.mapper.ImParentMapper;
import com.jeramtough.im.model.dto.ImParentDto;
import com.jeramtough.im.model.dto.ImParentWithStudentDto;
import com.jeramtough.im.model.dto.ImStudentDto;
import com.jeramtough.im.model.entity.ImParent;
import com.jeramtough.im.model.params.AddOrUpdateParentParams;
import com.jeramtough.im.service.ImParentService;
import com.jeramtough.im.service.ImStudentService;
import com.jeramtough.im.service.base.impl.MyBaseServiceImpl;
import com.jeramtough.jtweb.component.apiresponse.error.ErrorCode;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiException;
import com.jeramtough.jtweb.component.apiresponse.exception.ApiResponseException;
import com.jeramtough.jtweb.component.validation.BeanValidator;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.error.ErrorU;
import com.jeramtough.jtweb.model.params.BaseConditionParams;
import com.jeramtough.jtweb.model.params.QueryByPageParams;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Objects;

/**
 * <pre>
 * Created on 2021/9/17 下午10:11
 * by @author WeiBoWen
 * </pre>
 */
@Service
public class ImParentServiceImpl
        extends MyBaseServiceImpl<ImParentMapper, ImParent, ImParentDto>
        implements ImParentService {

    private final ImStudentService imStudentService;

    public ImParentServiceImpl(WebApplicationContext wc,
                               ImStudentService imStudentService) {
        super(wc);
        this.imStudentService = imStudentService;
    }

    @Override
    public PageDto<ImParentDto> pageByConditionTwo(QueryByPageParams queryByPageParams,
                                                   BaseConditionParams params,
                                                   QueryWrapper<ImParent> queryWrapper) {

        if (StringUtils.hasText(params.getKeyword())) {
            queryWrapper.like("name", params.getKeyword());
        }

        //查询关联的学生
        List<ImStudentDto> studentDtoList =
                imStudentService.getListByKeyword(params.getKeyword());
        if (!studentDtoList.isEmpty()) {
            for (ImStudentDto studentDto : studentDtoList) {
                queryWrapper.or().eq("student_id", studentDto.getFid());
            }
        }


        //使用查询权限过滤器
        queryWrapper =
                getWC().getBean(ParentQueryPermissionFilter.class).doFilter(
                        queryWrapper);

        return super.pageByConditionTwo(queryByPageParams, params, queryWrapper);
    }

    @Override
    public String addParent(AddOrUpdateParentParams params) {
        BeanValidator.verifyParams(params);

        ImParent imParent = getMapperFacade().map(params, ImParent.class);

        if (StringUtils.hasText(params.getStudentNumberOrName())) {
            try {

                ImStudentDto studentDto = imStudentService.getByNumberOrName(
                        params.getStudentNumberOrName());
                Objects.requireNonNull(studentDto);
                imParent.setStudentId(studentDto.getFid());
            }
            catch (ApiResponseException apiResponseException) {
                if (apiResponseException.getCode() == ErrorU.CODE_9.C) {
                    throw new ApiResponseException(ErrorU.CODE_10.C, "学号", "学生对象");
                }
                else {
                    throw apiResponseException;
                }
            }
            catch (IllegalStateException e) {
                throw new ApiException(ErrorCode.U, "存在同名情况！请换成输入学生学号");
            }

        }


        //操作者id
        Long operatorUid = UserHolder.getSystemUser().getUid();
        imParent.setOperatorUid(operatorUid);

        return addByParams(imParent);
    }

    @Override
    public String updateParent(AddOrUpdateParentParams params) {

        ImParent oldImParent = getById(params.getFid());
        ImParent imParent = getMapperFacade().map(params, ImParent.class);

        if (StringUtils.hasText(params.getStudentNumberOrName())) {
            try {
                ImStudentDto studentDto = imStudentService.getByNumberOrName(
                        params.getStudentNumberOrName());
                Objects.requireNonNull(studentDto);
                ImStudentDto oldStudentDto =
                        imStudentService.getBaseDtoById(oldImParent.getStudentId());

                boolean needModifiedStudentNumber =
                        (!oldStudentDto.getFid().equals(studentDto.getFid()));
                if (needModifiedStudentNumber) {
                    imParent.setStudentId(studentDto.getFid());
                }
            }
            catch (ApiResponseException apiResponseException) {
                if (apiResponseException.getCode() == ErrorU.CODE_9.C) {
                    throw new ApiResponseException(ErrorU.CODE_10.C, "学号", "学生对象");
                }
                else {
                    throw apiResponseException;
                }
            }
            catch (IllegalStateException e) {
                throw new ApiException(ErrorCode.U, "存在同名情况！请换成输入学生学号");
            }

        }

        return updateByParams(imParent);
    }

    @Override
    public List<ImParentWithStudentDto> getAll() {

        //使用查询权限过滤器
        ParentQueryPermissionFilter queryPermissionFilter =
                getWC().getBean(ParentQueryPermissionFilter.class);

        List<ImParentWithStudentDto> list =
                getBaseMapper().selectAllParentWithStudent(
                        queryPermissionFilter.getFilterSql("im_parent"));
        return list;
    }
}
