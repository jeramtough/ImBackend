package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImStudentScoreDto;
import com.jeramtough.im.model.entity.ImStudentScore;
import com.jeramtough.im.model.params.AddOrUpdateStudentParams;
import com.jeramtough.im.model.params.AddOrUpdateStudentScoreParams;
import com.jeramtough.im.model.params.StudentScoreConditionParams;
import com.jeramtough.im.service.base.MyBaseService;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-20
 */
public interface ImStudentScoreService
        extends MyBaseService<ImStudentScore, ImStudentScoreDto> {

    String updateStudentScore(List<AddOrUpdateStudentScoreParams> params);

    PageDto<ImStudentScoreDto> pageByStudentScoreCondition(QueryByPageParams queryByPageParams,
                                                           StudentScoreConditionParams conditionParams);
}
