package com.jeramtough.im.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jeramtough.im.component.userdetail.UserHolder;
import com.jeramtough.im.service.UserService;
import com.jeramtough.randl2.sdk.model.httpresponse.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <pre>
 * Created on 2021/8/17 下午10:58
 * by @author WeiBoWen
 * </pre>
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserServiceImpl() {
    }

    @Override
    public Map<String, Object> getInfo() {
        SystemUser systemUser = UserHolder.getSystemUser();
        String json = JSON.toJSONString(systemUser);
        JSONObject jsonObject = JSON.parseObject(json);
        return jsonObject;
    }
}
