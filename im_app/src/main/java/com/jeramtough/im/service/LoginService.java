package com.jeramtough.im.service;

import com.jeramtough.im.model.params.UserCredentials;

import java.util.Map;

/**
 * <pre>
 * Created on 2021/8/17 下午10:01
 * by @author WeiBoWen
 * </pre>
 */
public interface LoginService {
    Map<String, Object> login(UserCredentials userCredentials);
}
