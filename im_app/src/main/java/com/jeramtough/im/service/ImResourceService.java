package com.jeramtough.im.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <pre>
 * Created on 2021/9/15 上午10:39
 * by @author WeiBoWen
 * </pre>
 */
public interface ImResourceService {

    Map<String, Object> uploadSurfaceImage(MultipartFile file);

    void readSurfaceImage(HttpServletResponse response, String imagePath);

    boolean deleteHeadimageByPath(String imagePath);


    String readSurfaceImageForBase64(String imagePath);
}
