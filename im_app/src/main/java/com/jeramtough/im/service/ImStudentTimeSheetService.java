package com.jeramtough.im.service;

import com.jeramtough.im.model.dto.ImStudentTimeSheetDto;
import com.jeramtough.im.model.entity.ImStudentTimeSheet;
import com.jeramtough.im.model.params.AddOrUpdateImStudentTimeSheetParams;
import com.jeramtough.im.model.params.StudentTimeSheetConditionParams;
import com.jeramtough.im.service.base.MyBaseService;
import com.jeramtough.jtweb.model.dto.PageDto;
import com.jeramtough.jtweb.model.params.QueryByPageParams;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WeiBoWen
 * @since 2021-09-22
 */
public interface ImStudentTimeSheetService extends MyBaseService<ImStudentTimeSheet,
        ImStudentTimeSheetDto> {

    String updateStudentTimeSheet(List<AddOrUpdateImStudentTimeSheetParams> params);

    PageDto<Map<String, Object>> pageByStudentTimeSheetCondition(
            QueryByPageParams queryByPageParams,
            StudentTimeSheetConditionParams conditionParams);

    List<String> getDayKeys(Date date);

    List<ImStudentTimeSheet> getImStudentTimeSheetsByStudentIdAndDate(
            Date date, List<Long> studentIds);
}
